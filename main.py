import discord
from discord.ext import commands
import random
import urllib.request, json 
import sqlite3
import os

description = '''Taktikek bot'''
bot  = commands.Bot(command_prefix='`', description=description)
conn = sqlite3.connect("taktikek.db")
cursor = conn.cursor()
cursor.execute("""  CREATE TABLE IF NOT EXISTS `bookmarks` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,`url` varchar(255) NOT NULL)""")
conn.commit()
cursor = conn.cursor()
cursor.execute("""  CREATE TABLE IF NOT EXISTS `functions` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,`todo` varchar(255) NOT NULL)""")
conn.commit()

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command()
async def addb(ctx, url):
    """Добавление ссылки в закладки"""
    cursor = conn.cursor()
    cursor.execute("INSERT INTO bookmarks (url) VALUES (?)", [(url)])
    conn.commit()
    await ctx.send("ссылка добавленна")

@bot.command()
async def addtodo(ctx, value):
    """todo запрос функций где value текст в кавычках с запросом на fix или на новые функции"""
    cursor = conn.cursor()
    cursor.execute("INSERT INTO functions (todo) VALUES (?)", [(value)])
    conn.commit()
    await ctx.send("запрос добавлен")

@bot.command()
async def showtodo(ctx):
    """ вывод todo  """
    cursor = conn.cursor()
    sql = "SELECT * FROM functions"
    cursor.execute(sql)
    res = cursor.fetchall()
    
    ids  = []
    vals = []

    for i in range(0, len(res)):
        ids  = res[i][0]
        vals = res[i][1]
        await ctx.send( '```id: {0}{1}text: {2}```'.format(str(res[i][0]),'\n', str(res[i][1]) ))

@bot.command()
async def deltodo(ctx, id):
    """Удалить todo где id равняется выбранного todo"""
    cursor = conn.cursor()
    cursor.execute("DELETE FROM bookmarks WHERE id = ?", [(int(id))])
    conn.commit()
    await ctx.send("ссылка удаленна")

@bot.command()
async def showb(ctx):
    """Выводит закладки"""
    cursor = conn.cursor()
    sql = "SELECT * FROM bookmarks"
    cursor.execute(sql)
    res = cursor.fetchall()
    
    ids  = []
    urls = []

    for i in range(0, len(res)):
        ids  = res[i][0]
        urls = res[i][1]
        await ctx.send('```id: {0}{1}url: {2}```'.format(str(res[i][0]),'\n', str(res[i][1]) ))

@bot.command()
async def delb(ctx, id):
    """Удалить закладку"""
    cursor = conn.cursor()
    cursor.execute("DELETE FROM bookmarks WHERE id = ?", [(int(id))])
    conn.commit()
    await ctx.send("ссылка удаленна")

@bot.command()
async def mc_restart(ctx, m_size): #size с определенным количеством памяти
    """Перезапустить сервер с параметром m_size - где m_size колличество памяти. 
        Например: 2024 - для двух гигабай, 3024 для трех и тд"""
    if isinstance(int(m_size), int):
        os.system('screen -S taktikek -X quit')
        cm = 'screen -dm -S taktikek java -Xmx' + m_size + 'M -Xms' + m_size + 'M -jar forge-1.14.4-28.1.0.jar nogui'
        os.system(cm)
        await ctx.send("сервер перезапущен")
        with open('mine.jpg', 'rb') as fp:
            await ctx.send(file=discord.File(fp, 'new_mine.jpg'))
    else:
        await ctx.send("сервер должен быть intenger или же цифрами")

@bot.command()
async def release(ctx):
    """Выводит релиз программы. Преффикс '-' значит что релиз еще не в релиз на гите"""
    await ctx.send("``` release: {}{} github: {} ```".format('alpha 0.0.1-2', '\n', 'https://github.com/mar4elkin/taktikek_discord_bot/releases'))


@bot.command()
async def mc_stats(ctx):
    """Выводит информацию о minecraft сервере."""
    with urllib.request.urlopen("https://api.mcsrvstat.us/ping/95.161.152.122:25565") as url:
        data = json.loads(url.read().decode())
        moto = data['description']['text']
        players = data['players']['online']
        players_list = []
        
        if players > 0:
            for i in range(0, players):
                players_list.append(data['players']['sample'][i]['name'])
            
            await ctx.send('```moto: {0}, plyers online: {1}{2}players: {3} ```'.format(moto, players,'\n', str(players_list) ))
            

        else:
            await ctx.send('``` moto: {0}, plyers online: {1} ```'.format(moto, players)) 
        






bot.run('TOKEN')
